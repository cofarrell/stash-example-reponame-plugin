package com.atlassian.stash.plugin.reponame;

import com.atlassian.event.api.EventListener;
import com.atlassian.stash.event.RepositoryCreatedEvent;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.util.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import java.io.File;
import java.io.IOException;

public class RepositoryListener implements InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(RepositoryListener.class);

    private static final String NAME_FILE = "name";

    private final ApplicationPropertiesService propertiesService;
    private final RepositoryService repositoryService;
    private final SecurityService securityService;

    public RepositoryListener(ApplicationPropertiesService propertiesService, RepositoryService repositoryService, SecurityService securityService) {
        this.propertiesService = propertiesService;
        this.repositoryService = repositoryService;
        this.securityService = securityService;
    }

    @EventListener
    public void listener(RepositoryCreatedEvent event) throws IOException {
        // When a new repository is created create the 'name' file
        updateRepository(event.getRepository());
    }

    /**
     * Update all the repositories on startup (this might be after the plugin is installed the first time).
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Initialising all repository names");
        // We need to run with REPO_READ permissions
        securityService.doWithPermission("Reading all repositories for their slugs", Permission.REPO_READ, new Operation<Object, IOException>() {
            @Override
            public Object perform() throws IOException {
                Iterable<Repository> repos = new PagedIterable<Repository>(new PageProvider<Repository>() {
                    @Override
                    public Page<Repository> get(PageRequest pageRequest) {
                        return (Page) repositoryService.findAll(pageRequest);
                    }
                }, new PageRequestImpl(0, 100));
                for (Repository repository : repos) {
                    updateRepository(repository);
                }
                return null;
            }
        });
    }

    private void updateRepository(final Repository repository) throws IOException {
        log.info("Updating repository name {}", repository.getSlug());
        File repositoryDir = propertiesService.getRepositoryDir(repository);
        FileUtils.writeStringToFile(new File(repositoryDir, NAME_FILE), repository.getSlug());
    }
}
